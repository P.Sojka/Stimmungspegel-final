/**
 * Created by Patrick-Sojka on 26.06.17.
 */
function base64toBlob(base64Data) {

    var byteString = atob(base64Data);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);

    for(var i=0; i< byteString.length;i++){
        ia[i]=byteString.charCodeAt(i);
    }
    return new Blob([ab],{type: 'audio/wav'});
}