# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table location (
  id                            bigint auto_increment not null,
  cname                         varchar(255),
  bierpreis                     double,
  stimmung                      integer,
  audio                         varchar(255),
  eintritt                      double,
  constraint pk_location primary key (id)
);


# --- !Downs

drop table if exists location;

