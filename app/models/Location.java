package models;

import com.avaje.ebean.Model;

import javax.persistence.*;




@Entity
public class Location extends Model{

    @Id
    @Column
    public Long id;

    @Column
    private String CName;

    @Column
    private double Bierpreis;

    @Column
    private int Stimmung;

    @Column
    private String audio;

    @Column
    private double eintritt;

    public Location() {
    }

    public Location(Long id, String CName, double bierpreis, int stimmung, String audio, double eintritt) {
        this.id = id;
        this.CName = CName;
        Bierpreis = bierpreis;
        Stimmung = stimmung;
        this.audio = audio;
        this.eintritt = eintritt;
    }

    public static Finder<Long, Location> find = new Finder<Long,Location>(Location.class);

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCName() {
        return CName;
    }

    public void setCName(String CName) {
        this.CName = CName;
    }

    public double getBierpreis() {
        return Bierpreis;
    }

    public void setBierpreis(double bierpreis) {
        Bierpreis = bierpreis;
    }

    public int getStimmung() {
        return Stimmung;
    }

    public void setStimmung(int stimmung) {
        Stimmung = stimmung;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public double getEintritt() {
        return eintritt;
    }

    public void setEintritt(double eintritt) {
        this.eintritt = eintritt;
    }

    public static Finder<Long, Location> getFind() {
        return find;
    }

    public static void setFind(Finder<Long, Location> find) {
        Location.find = find;
    }
}