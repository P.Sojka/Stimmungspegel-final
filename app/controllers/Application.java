package controllers;

import com.avaje.ebean.Ebean;
import models.Location;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.*;
import views.html.*;
import java.util.List;



public class Application extends Controller {

    public Result index() {
        List<Location> clublist = Location.find.all();
        return ok(views.html.index.render(clublist));
    }


    public Result addLocation(){
        Location location = new Location();
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        if(dynamicForm.get("Club2tags2") == null){
            location.setCName(dynamicForm.get("Club2tags200"));
        }
        else {
            location.setCName(dynamicForm.get("Club2tags2"));
        }
        return ok(views.html.addLocation.render(location));
    }


    public Result createLocationDesk(){
        boolean updateErfolgreich = false;
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        Location location = new Location();
        List<Location> clublist = Location.find.all();

        location.setCName(dynamicForm.get("nameClub"));
        location.setBierpreis(Double.parseDouble(dynamicForm.get("bier")));
        location.setEintritt(Double.parseDouble(dynamicForm.get("eintrittclub")));

        for(Location locAct : clublist){
            if(locAct.getCName().equals(location.getCName())){
                location.setId(locAct.getId());
                System.out.println("\n Speichere ID: "+locAct.getId());
                Ebean.update(location);
                clublist = Location.find.all();
                System.out.println("\n Aktualisiere die Liste!");
                updateErfolgreich = true;
                break;
            }
        }

        //Neues Objekt noch nicht in der DB gewesen -> der List hinzufügen und
        if(updateErfolgreich == false){
            System.out.println("\n INSERT");
            location.save();
        }

        clublist.add(location);
        return ok(index.render(clublist));
    }


    public Result createLocationResp(){
        boolean updateErfolgreich = false;
        List<Location> clublist = Location.find.all();
        Location location = new Location();
        DynamicForm dynamicForm = Form.form().bindFromRequest();
        location.setCName(dynamicForm.get("CName"));
        location.setBierpreis(Double.parseDouble(dynamicForm.get("bierMobil")));
        location.setEintritt(Double.parseDouble(dynamicForm.get("eintrittMobil")));
        location.setStimmung(Integer.parseInt(dynamicForm.get("stimmung")));

        //Audio
        String str = dynamicForm.get("dateiaudio");
        String[] strsplited = str.split(",");
        String b64 = strsplited[1];
        location.setAudio(b64);

        for(Location locAct : clublist){
            if(locAct.getCName().equals(location.getCName())){
                location.setId(locAct.getId());
                System.out.println("\n Speichere ID: "+locAct.getId());
                Ebean.update(location);
                System.out.println("\n Aktualisiere die Liste!");
                clublist = Location.find.all();
                updateErfolgreich = true;
            }
        }

        //Neues Objekt noch nicht in der DB gewesen -> der List hinzufügen und
        if(updateErfolgreich == false) {
            System.out.println("\n INSERT");
            location.save();
        }

        clublist.add(location);
        return ok(index.render(clublist));
    }
}
